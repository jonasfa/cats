# Cats

[![pipeline status](https://gitlab.com/jonasfa/cats/badges/master/pipeline.svg)](https://gitlab.com/jonasfa/cats/commits/master)

**Cats** is an Android app for cat lovers, where you can browse breeds and cats!

# Repository

The *master* branch is the stable branch. It should be releasable at all times.

All development happens on feature branches. Once development is finished, the branch author should open a [Merge Request](https://gitlab.com/jonasfa/cats/merge_requests). Once all eventual issues are resolved, it may be merged.

Releases should be *tagged*.

# Tech

This application was built with the **MVVM** architecture pattern in mind, specifically with the [Android Jetpack ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel). You'll find `ViewModel` subclasses for each Activity that performs costly operations for loading content, like network requests.

There are **automated tests** for all features. When a test depends on Android framework classes, it's written as an Instrumentation test. Otherwise, as a simple unit test. Finally, UI tests use *Espresso* to interact with and verify the UI.

**Cats** uses a number of open source projects:

* [Kotlin](https://kotlinlang.org/) - the programming language
* [Android Jetpack](https://developer.android.com/jetpack) - incl. support libraries, RecyclerView, Espresso, Room and more
* [Retrofit](https://square.github.io/retrofit/) - for consuming REST web services
* [RxJava](https://github.com/ReactiveX/RxJava) - for powerful concurrency and transforming streams of data
* [Picasso](https://square.github.io/picasso/) - straightforward image loading
* [Mockito](https://site.mockito.org/) - allows focused unit tests by impersonating other objects and verifying interactions with them
* [OkHttp's mockwebserver](https://github.com/square/okhttp/tree/master/mockwebserver) - to make sure the whole networking stack is working as expected, from connections to (de)serializing

## Tracking

Because a UI component shouldn't be responsible for tracking itself, this application's **tracking** module [initializes itself with a `ContentProvider`](https://firebase.googleblog.com/2016/12/how-does-firebase-initialize-on-android.html) and then listens to screen views with an `ActivityLifecycleCallbacks`. This means the application module doesn't need to call any tracking APIs, or even initialize tracking manually. Instead, it only provides metadata by implementing a small interface.

## Continuous Integration

[CI Pipelines](https://gitlab.com/jonasfa/cats/pipelines) are automatically started every time changes are pushed to the repository. The push author will be notified of unsuccessful jobs.

#### Building
Syntactic checks (lint) and unit tests:
```sh
$ ./gradlew check
```
Instrumentation tests, including UI tests:
```sh
$ ./gradlew connectedAndroidTest
```
Assemble the APK
```sh
$ ./gradlew app:assembleDebug
```
