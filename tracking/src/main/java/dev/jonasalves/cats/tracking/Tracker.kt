package dev.jonasalves.cats.tracking

import dev.jonasalves.cats.tracking.persistence.Event
import dev.jonasalves.cats.tracking.persistence.EventsDao
import io.reactivex.schedulers.Schedulers

internal open class Tracker(lazyDao: Lazy<EventsDao>) {
    private val eventsDao by lazyDao

    open fun track(trackable: Trackable) {
        eventsDao.insertEvent(trackable.toEvent())
            .subscribeOn(Schedulers.io())
            .onErrorComplete()
            .subscribe()
    }
}

private fun Trackable.toEvent() =
    trackableTypeAndId.let { (type, id) -> Event(null, type, id) }
