package dev.jonasalves.cats.tracking

interface Trackable {
    val trackableTypeAndId: Pair<String, String>
}
