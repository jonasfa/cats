package dev.jonasalves.cats.tracking

import android.app.Application
import android.content.ContentProvider
import android.content.ContentValues
import android.net.Uri
import androidx.room.Room
import dev.jonasalves.cats.tracking.persistence.TrackingDatabase

/**
 * Initializes tracking early in the Application lifecycle.
 *
 * @see <a href="https://firebase.googleblog.com/2016/12/how-does-firebase-initialize-on-android.html">
 *     How does Firebase initialize on Android?</a>
 */
class TrackingInitializer : ContentProvider() {
    override fun onCreate(): Boolean {
        val tracker = Tracker(lazy { buildEventsDao() })
        (context!!.applicationContext as Application)
            .registerActivityLifecycleCallbacks(TrackingActivityCallback(tracker))

        return true
    }

    private fun buildEventsDao() =
        Room.databaseBuilder(context!!, TrackingDatabase::class.java, "tracking.sqlite")
            .build()
            .eventsDao()

    override fun getType(uri: Uri) = throw UnsupportedOperationException()
    override fun insert(uri: Uri, values: ContentValues) = throw UnsupportedOperationException()
    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?) =
        throw UnsupportedOperationException()

    override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<String>?) =
        throw UnsupportedOperationException()

    override fun query(
        uri: Uri,
        projection: Array<String>?,
        selection: String?,
        selectionArgs: Array<String>?,
        sortOrder: String?
    ) = throw UnsupportedOperationException()
}
