package dev.jonasalves.cats.tracking

import android.app.Activity
import android.app.Application
import android.os.Bundle

internal class TrackingActivityCallback(private val tracker: Tracker) : Application.ActivityLifecycleCallbacks {
    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        (activity as? Trackable)
            ?.takeIf { savedInstanceState == null }
            ?.let(tracker::track)
    }

    override fun onActivityResumed(activity: Activity) = Unit
    override fun onActivityStarted(activity: Activity) = Unit
    override fun onActivityPaused(activity: Activity) = Unit
    override fun onActivityStopped(activity: Activity) = Unit
    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle) = Unit
    override fun onActivityDestroyed(activity: Activity) = Unit
}
