package dev.jonasalves.cats.tracking.persistence

import androidx.room.Dao
import androidx.room.Insert
import io.reactivex.Completable

@Dao
internal interface EventsDao {
    @Insert
    fun insertEvent(event: Event): Completable
}
