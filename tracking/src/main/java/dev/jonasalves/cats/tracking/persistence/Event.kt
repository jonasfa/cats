package dev.jonasalves.cats.tracking.persistence

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
internal data class Event(
    @PrimaryKey(autoGenerate = true) var id: Int?,
    var contentType: String,
    var contentId: String
)
