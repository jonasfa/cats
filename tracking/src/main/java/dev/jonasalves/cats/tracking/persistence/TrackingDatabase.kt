package dev.jonasalves.cats.tracking.persistence

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Event::class], version = 1)
internal abstract class TrackingDatabase : RoomDatabase() {
    abstract fun eventsDao(): EventsDao
}