package dev.jonasalves.cats.tracking

import android.app.Application
import android.content.Context
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.isA
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.reset
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Test

class TrackingInitializerTest {
    val subject = TrackingInitializer()

    val application = mock<Application>()
    val context = mock<Context> {
        on { applicationContext } doReturn application
    }

    @Before
    fun attachInfo() {
        subject.attachInfo(context, null)
        reset(application) // attachInfo calls onCreate
    }

    @Test
    fun onCreate() {
        subject.onCreate()

        verify(application).registerActivityLifecycleCallbacks(isA<TrackingActivityCallback>())
    }
}
