package dev.jonasalves.cats.tracking

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import dev.jonasalves.cats.tracking.persistence.Event
import dev.jonasalves.cats.tracking.persistence.EventsDao
import io.reactivex.Completable
import org.junit.Assert.assertEquals
import org.junit.Test

internal class TrackerTest {
    var insertSubscriptions = 0

    val eventsDao = mock<EventsDao> {
        on { insertEvent(Event(null, "category", "abc")) } doReturn
                Completable.complete().doOnSubscribe { insertSubscriptions++ }
    }
    val subject = Tracker(lazy { eventsDao })

    @Test
    fun track() {
        subject.track(Pair("category", "abc").toTrackable())

        Thread.sleep(20) // the subscription is created on another thread
        assertEquals(1, insertSubscriptions)
    }
}

private fun Pair<String, String>.toTrackable() = object : Trackable {
    override val trackableTypeAndId = this@toTrackable
}
