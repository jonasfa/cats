package dev.jonasalves.cats.tracking

import android.app.Activity
import android.os.Bundle
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import org.junit.Test

internal class TrackingActivityCallbackTest {
    val tracker = mock<Tracker>()
    val subject = TrackingActivityCallback(tracker)

    val trackableActivity: Activity = object : Activity(), Trackable {
        override val trackableTypeAndId = Pair("category", "abc")
    }

    @Test
    fun onActivityCreated_withTrackableActivity_tracks() {
        subject.onActivityCreated(trackableActivity, null)

        verify(tracker).track(trackableActivity as Trackable)
    }

    @Test
    fun onActivityRecreated_doesNothing() {
        subject.onActivityCreated(trackableActivity, Bundle())

        verifyNoMoreInteractions(tracker)
    }

    @Test
    fun onActivityCreated_withPlainActivity_doesNothing() {
        subject.onActivityCreated(Activity(), null)

        verifyNoMoreInteractions(tracker)
    }
}
