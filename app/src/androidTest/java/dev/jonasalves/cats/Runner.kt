package dev.jonasalves.cats

import android.app.Instrumentation
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner

@Suppress("unused")
class Runner : AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader, className: String, context: Context): android.app.Application {
        return Instrumentation.newApplication(TestApplication::class.java, context)
    }
}
