package dev.jonasalves.cats

@Suppress("Unused")
class TestApplication : Application() {
    override val baseUrl = "http://localhost:9999/"
}
