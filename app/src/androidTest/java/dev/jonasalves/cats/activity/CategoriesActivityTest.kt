package dev.jonasalves.cats.activity

import android.content.ComponentName
import android.widget.ProgressBar
import androidx.lifecycle.ViewModelProviders
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.matcher.IntentMatchers.hasExtra
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import dev.jonasalves.cats.content.CategoryVisitsCounter
import dev.jonasalves.cats.model.Category
import dev.jonasalves.cats.rules.MockServerRule
import dev.jonasalves.cats.rules.onCreateRule
import dev.jonasalves.cats.viewmodel.CategoriesViewModel
import okhttp3.mockwebserver.MockResponse
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CategoriesActivityTest {
    @get:Rule
    val mockServerRule = MockServerRule()

    @get:Rule
    val activityRule = IntentsTestRule(CategoriesActivity::class.java, true)

    @get:Rule
    val onCreateRule = onCreateRule<CategoriesActivity> {
        categoryVisitCounter = this@CategoriesActivityTest.categoryVisitsCounter
    }

    val exampleCategories = listOf(
        Category("bs", "British Shorthair"),
        Category("pc", "Persian Cat"),
        Category("mc", "Minchkin Cat")
    )

    val categoryVisitsCounter = mock<CategoryVisitsCounter>()

    val viewModel get() = ViewModelProviders.of(activityRule.activity).get(CategoriesViewModel::class.java)

    @Before
    fun registerIdlingResource() {
        IdlingRegistry.getInstance().register(viewModel.idlingResource)
    }

    @After
    fun unregisterIdlingResource() {
        IdlingRegistry.getInstance().unregister(viewModel.idlingResource)
    }

    @Test
    fun showsProgressWhileLoading() {
        unregisterIdlingResource()

        onView(isAssignableFrom(ProgressBar::class.java))
            .check(matches(isDisplayed()))
    }

    @Test
    fun hideProgressWhenLoaded() {
        mockServerRule.responses.add("/categories.json" to exampleCategories)

        onView(isAssignableFrom(ProgressBar::class.java))
            .check(matches(not(isDisplayed())))
    }

    @Test
    fun showsCategoriesWhenLoaded() {
        mockServerRule.responses.add("/categories.json" to exampleCategories)

        onView(withText("British Shorthair"))
            .check(matches(isDisplayed()))
    }

    @Test
    fun retriesLoadingOnErrors() {
        mockServerRule.responses.add("/categories.json" to MockResponse().setResponseCode(500))
        mockServerRule.responses.add("/categories.json" to exampleCategories)

        onView(withText("British Shorthair"))
            .check(matches(isDisplayed()))
    }

    @Test
    fun launchesCategoryItemsOnClick() {
        mockServerRule.responses.add("/categories.json" to exampleCategories)

        onView(withText("British Shorthair"))
            .perform(click())

        intended(
            allOf(
                hasComponent(ComponentName(activityRule.activity, CategoryItemsActivity::class.java)),
                hasExtra(CategoryItemsActivity.EXTRA_CATEGORY, exampleCategories.first())
            )
        )
    }

    @Test
    fun incrementsCategoryVisitsOnClick() {
        mockServerRule.responses.add("/categories.json" to exampleCategories)

        onView(withText("British Shorthair"))
            .perform(click())

        verify(categoryVisitsCounter).incrementVisits("bs")
    }

    @Test
    fun launchesMostVisitedCategory() {
        whenever(categoryVisitsCounter.visitsFor("bs")) doReturn 0
        whenever(categoryVisitsCounter.visitsFor("pc")) doReturn 7
        whenever(categoryVisitsCounter.visitsFor("mc")) doReturn 3

        mockServerRule.responses.add("/categories.json" to exampleCategories)

        intended(
            allOf(
                hasComponent(ComponentName(activityRule.activity, MostVisitedCategoryActivity::class.java)),
                hasExtra(MostVisitedCategoryActivity.EXTRA_CATEGORY, exampleCategories[1])
            )
        )
    }
}
