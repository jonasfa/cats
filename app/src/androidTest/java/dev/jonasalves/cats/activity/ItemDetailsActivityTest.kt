package dev.jonasalves.cats.activity

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withContentDescription
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import dev.jonasalves.cats.R
import dev.jonasalves.cats.model.Item
import org.junit.Rule
import org.junit.Test

class ItemDetailsActivityTest {
    @get:Rule
    val activityRule = object : ActivityTestRule<ItemDetailsActivity>(ItemDetailsActivity::class.java, true) {
        override fun getActivityIntent() =
            Intent().putExtra(ItemDetailsActivity.EXTRA_ITEM, Item("bs", "Cat bs1", null, "Meow bs1!"))
    }

    @Test
    fun populatesViews() {
        onView(withText("Cat bs1")).check(matches(isDisplayed()))

        onView(withContentDescription(activityRule.activity.getString(R.string.picture_content_description, "Cat bs1")))
            .check(matches(isDisplayed()))

        onView(withText("Meow bs1!")).check(matches(isDisplayed()))
    }
}