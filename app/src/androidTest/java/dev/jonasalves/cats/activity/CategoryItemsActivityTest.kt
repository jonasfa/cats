package dev.jonasalves.cats.activity

import android.content.ComponentName
import android.content.Intent
import android.widget.ProgressBar
import androidx.lifecycle.ViewModelProviders
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.matcher.IntentMatchers.hasExtra
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import dev.jonasalves.cats.rules.MockServerRule
import dev.jonasalves.cats.R
import dev.jonasalves.cats.model.Category
import dev.jonasalves.cats.model.Item
import dev.jonasalves.cats.viewmodel.CategoryItemsViewModel
import okhttp3.mockwebserver.MockResponse
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CategoryItemsActivityTest {
    @get:Rule
    val mockServerRule = MockServerRule()

    @get:Rule
    val activityRule = object : IntentsTestRule<CategoryItemsActivity>(CategoryItemsActivity::class.java, true) {
        override fun getActivityIntent() =
            Intent().putExtra(CategoryItemsActivity.EXTRA_CATEGORY, Category("bs", "British Shorthair"))
    }

    val exampleItems = listOf(
        Item("bs1", "Cat bs1", null, "Meow bs1!"),
        Item("bs2", "Cat bs2", null, "Meow bs2!"),
        Item("bs3", "Cat bs3", null, "Meow bs3!")
    )

    val viewModel get() = ViewModelProviders.of(activityRule.activity).get(CategoryItemsViewModel::class.java)

    @Before
    fun registerIdlingResource() {
        IdlingRegistry.getInstance().register(viewModel.idlingResource)
    }

    @After
    fun unregisterIdlingResource() {
        IdlingRegistry.getInstance().unregister(viewModel.idlingResource)
    }

    @Test
    fun showsHeaderWithCategoryName() {
        unregisterIdlingResource()

        onView(withText(activityRule.activity.getString(R.string.category_results, "British Shorthair")))
            .check(matches(isDisplayed()))
    }

    @Test
    fun showsProgressWhileLoading() {
        unregisterIdlingResource()

        Espresso.onView(ViewMatchers.isAssignableFrom(ProgressBar::class.java))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun hideProgressWhenLoaded() {
        mockServerRule.responses.add("/categories/bs.json" to exampleItems)

        Espresso.onView(ViewMatchers.isAssignableFrom(ProgressBar::class.java))
            .check(ViewAssertions.matches(not(isDisplayed())))
    }

    @Test
    fun showsItemsWhenLoaded() {
        mockServerRule.responses.add("/categories/bs.json" to exampleItems)

        onView(withText("Cat bs1"))
            .check(matches(isDisplayed()))
    }

    @Test
    fun retriesLoadingOnErrors() {
        mockServerRule.responses.add("/categories/bs.json" to MockResponse().setResponseCode(500))
        mockServerRule.responses.add("/categories/bs.json" to exampleItems)

        onView(withText("Cat bs1"))
            .check(matches(isDisplayed()))
    }

    @Test
    fun launchesItemDetailsOnClick() {
        mockServerRule.responses.add("/categories/bs.json" to exampleItems)

        onView(withText("Cat bs1"))
            .perform(ViewActions.click())

        intended(
            allOf(
                hasComponent(ComponentName(activityRule.activity, ItemDetailsActivity::class.java)),
                hasExtra(ItemDetailsActivity.EXTRA_ITEM, exampleItems.first())
            )
        )
    }
}