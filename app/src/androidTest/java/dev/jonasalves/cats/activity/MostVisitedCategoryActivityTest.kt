package dev.jonasalves.cats.activity

import android.content.ComponentName
import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.matcher.IntentMatchers.hasExtra
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withContentDescription
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import dev.jonasalves.cats.R
import dev.jonasalves.cats.content.CategoryVisitsCounter
import dev.jonasalves.cats.model.Category
import dev.jonasalves.cats.rules.MockServerRule
import dev.jonasalves.cats.rules.onCreateRule
import junit.framework.Assert.assertTrue
import org.hamcrest.CoreMatchers.allOf
import org.junit.Rule
import org.junit.Test

class MostVisitedCategoryActivityTest {
    @get:Rule
    val mockServerRule = MockServerRule()

    val categoryVisitsCounter = mock<CategoryVisitsCounter>()
    @get:Rule
    val onCreateRule = onCreateRule<CategoriesActivity> {
        categoryVisitCounter = this@MostVisitedCategoryActivityTest.categoryVisitsCounter
    }

    val category = Category("bs", "British Shorthair")

    @get:Rule
    val activityRule =
        object : IntentsTestRule<MostVisitedCategoryActivity>(MostVisitedCategoryActivity::class.java, true) {
            override fun getActivityIntent() =
                Intent().putExtra(MostVisitedCategoryActivity.EXTRA_CATEGORY, category)
        }

    @Test
    fun populatesViews() {
        onView(withText("British Shorthair")).check(matches(isDisplayed()))

        onView(
            withContentDescription(
                activityRule.activity.getString(
                    R.string.picture_content_description,
                    "British Shorthair"
                )
            )
        ).check(matches(isDisplayed()))
    }

    @Test
    fun launchesCategoryItemsOnClick() {
        onView(withText("British Shorthair"))
            .perform(click())

        intended(
            allOf(
                hasComponent(ComponentName(activityRule.activity, CategoryItemsActivity::class.java)),
                hasExtra(CategoryItemsActivity.EXTRA_CATEGORY, category)
            )
        )
    }

    @Test
    fun launchesCategories() {
        val activity = activityRule.activity

        onView(withText(R.string.go_to_categories))
            .perform(click())

        whenever(categoryVisitsCounter.visitsFor("bs")) doReturn 1
        mockServerRule.responses.add("/categories.json" to listOf(Category("bs", "British Shorthair")))
        Thread.sleep(50) // we don't have access to the CategoriesViewModel#idlingResource here

        onView(withText(R.string.go_to_categories))
            .check(doesNotExist())
        assertTrue("MostVisitedCategoryActivity should be finishing", activity.isFinishing)
    }
}
