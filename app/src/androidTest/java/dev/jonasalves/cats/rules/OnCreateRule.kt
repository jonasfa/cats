package dev.jonasalves.cats.rules

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

class OnCreateRule<T : Activity>(activityClass: Class<T>, onCreate: T.() -> Unit) : TestRule {
    override fun apply(base: Statement, description: Description) = object : Statement() {
        override fun evaluate() {
            (InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application).apply {
                try {
                    registerActivityLifecycleCallbacks(activityLifecycleCallbacks)
                    base.evaluate()
                } finally {
                    unregisterActivityLifecycleCallbacks(activityLifecycleCallbacks)
                }
            }
        }
    }

    val activityLifecycleCallbacks = object : Application.ActivityLifecycleCallbacks {
        override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
            activity.takeIf(activityClass::isInstance)
                ?.let { activityClass.cast(it) }
                ?.let(onCreate)
        }

        override fun onActivityStarted(activity: Activity) = Unit
        override fun onActivityResumed(activity: Activity) = Unit
        override fun onActivityPaused(activity: Activity) = Unit
        override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) = Unit
        override fun onActivityStopped(activity: Activity) = Unit
        override fun onActivityDestroyed(activity: Activity) = Unit
    }
}

inline fun <reified T : Activity> onCreateRule(noinline onCreate: T.() -> Unit) =
    OnCreateRule(T::class.java, onCreate)
