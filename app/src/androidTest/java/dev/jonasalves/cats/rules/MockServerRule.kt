package dev.jonasalves.cats.rules

import com.google.gson.GsonBuilder
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import java.util.Collections.synchronizedList
import java.util.concurrent.TimeoutException

class MockServerRule : TestRule {
    /**
     * Map where keys are paths and values are response bodies.
     */
    val responses: MutableList<Pair<String, Any>> = synchronizedList(mutableListOf())

    override fun apply(base: Statement, description: Description): Statement =
        MockServerStatement(base)

    private inner class MockDispatcher : Dispatcher() {
        private val gson = GsonBuilder().create()

        override fun dispatch(request: RecordedRequest): MockResponse {
            return try {
                waitForResponse(request.path!!).let {
                    it as? MockResponse
                        ?: MockResponse().setResponseCode(200).setBody(gson.toJson(it))
                }
            } catch (e: TimeoutException) {
                MockResponse()
                    .setResponseCode(504)
            }
        }

        @Throws(TimeoutException::class)
        private fun waitForResponse(path: String): Any {
            val timeout = 1000
            val start = System.currentTimeMillis()

            while (System.currentTimeMillis() < start + timeout) {
                responses.firstOrNull { (expectedPath, _) -> path == expectedPath }
                    ?.takeIf { responses.remove(it) }
                    ?.let { (_, response) ->
                        return response
                    }

                Thread.sleep(10)
            }

            throw TimeoutException("There's no response for $path")
        }
    }

    private inner class MockServerStatement(private val base: Statement) : Statement() {
        private val mockServer = MockWebServer().apply { dispatcher = MockDispatcher() }

        override fun evaluate() {
            mockServer.start(9999)

            try {
                base.evaluate()
            } catch (e: Exception) {
                throw e
            } finally {
                mockServer.shutdown()
                responses.clear()
            }
        }
    }
}
