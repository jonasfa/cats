package dev.jonasalves.cats.injection

import dev.jonasalves.cats.content.CategoryVisitsCounter
import dev.jonasalves.cats.viewmodel.CategoriesViewModel
import dev.jonasalves.cats.viewmodel.CategoryItemsViewModel

interface NeedsCategoryVisitsCounter {
    var categoryVisitCounter: CategoryVisitsCounter
}

interface NeedsCategoriesViewModel {
    var categoriesViewModel: CategoriesViewModel
}

interface NeedsCategoryItemsViewModel {
    var categoryItemsViewModel: CategoryItemsViewModel
}
