package dev.jonasalves.cats.injection

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory
import androidx.lifecycle.ViewModelProviders
import dev.jonasalves.cats.ContentApi
import dev.jonasalves.cats.content.CategoryVisitsCounter
import dev.jonasalves.cats.viewmodel.CategoriesViewModel
import dev.jonasalves.cats.viewmodel.CategoryItemsViewModel
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

class InjectionActivityCallbacks(private val application: dev.jonasalves.cats.Application) :
    Application.ActivityLifecycleCallbacks {
    private val categoryVisitsCounter = CategoryVisitsCounter()
    private val viewModelFactory = InjectionViewModelFactory()

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        (activity as? NeedsCategoryVisitsCounter)?.categoryVisitCounter = categoryVisitsCounter
        (activity as FragmentActivity).let {
            (activity as? NeedsCategoriesViewModel)?.categoriesViewModel = getFromProvider(activity)
            (activity as? NeedsCategoryItemsViewModel)?.categoryItemsViewModel = getFromProvider(activity)
        }
    }

    private inline fun <reified T : ViewModel> getFromProvider(activity: FragmentActivity) =
        ViewModelProviders.of(activity, viewModelFactory).get(T::class.java)

    override fun onActivityStarted(activity: Activity) = Unit
    override fun onActivityResumed(activity: Activity) = Unit
    override fun onActivityPaused(activity: Activity) = Unit
    override fun onActivityStopped(activity: Activity) = Unit
    override fun onActivityDestroyed(activity: Activity) = Unit
    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) = Unit

    /**
     * Builds [ViewModel] instances with application dependencies. If the [ViewModel] is unsupported, falls back to
     * [AndroidViewModelFactory].
     */
    private inner class InjectionViewModelFactory : ViewModelProvider.Factory {
        private val api by lazy {
            Retrofit.Builder()
                .baseUrl(application.baseUrl)
                .client(OkHttpClient.Builder().build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
                .create<ContentApi>()
        }

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            @Suppress("UNCHECKED_CAST")
            return when (modelClass) {
                CategoriesViewModel::class.java -> CategoriesViewModel(api)
                CategoryItemsViewModel::class.java -> CategoryItemsViewModel(api)
                else -> AndroidViewModelFactory.getInstance(application).create(modelClass)
            } as T
        }
    }
}
