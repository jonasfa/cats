package dev.jonasalves.cats.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.test.espresso.idling.CountingIdlingResource
import dev.jonasalves.cats.ContentApi
import dev.jonasalves.cats.model.Category
import dev.jonasalves.cats.postToNextFrame
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

class CategoriesViewModel(private val api: ContentApi) : ViewModel() {
    val idlingResource = CountingIdlingResource("${javaClass.simpleName} ${hashCode()}")

    private val _categories by lazy {
        MutableLiveData<List<Category>>().also { loadCategories() }
    }

    private var disposable: Disposable? = null

    val categories: LiveData<List<Category>> get() = _categories

    private fun loadCategories() {
        idlingResource.increment()

        disposable = api.categories()
            .retryWhen { it.delay(1, TimeUnit.SECONDS) }
            .subscribe { categories ->
                _categories.postValue(categories)

                // Data Binding delays UI updates to the next frame, so let's also delay decrementing the IdlingResource
                postToNextFrame(idlingResource::decrement)
            }
    }

    override fun onCleared() {
        super.onCleared()

        disposable?.dispose()
    }
}
