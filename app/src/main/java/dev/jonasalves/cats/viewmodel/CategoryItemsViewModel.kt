package dev.jonasalves.cats.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.test.espresso.idling.CountingIdlingResource
import dev.jonasalves.cats.ContentApi
import dev.jonasalves.cats.model.Category
import dev.jonasalves.cats.model.Item
import dev.jonasalves.cats.postToNextFrame
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates

class CategoryItemsViewModel(private val api: ContentApi) : ViewModel() {
    val idlingResource = CountingIdlingResource("${javaClass.simpleName} ${hashCode()}")

    private val _items by lazy {
        MutableLiveData<List<Item>>().also { loadItems() }
    }

    private var disposable: Disposable? = null
    var category by Delegates.notNull<Category>()

    val items: LiveData<List<Item>> get() = _items

    private fun loadItems() {
        idlingResource.increment()

        disposable = api.items(category.id)
            .retryWhen { it.delay(1, TimeUnit.SECONDS) }
            .subscribe { items ->
                _items.postValue(items)

                // Data Binding delays UI updates to the next frame, so let's also delay decrementing the IdlingResource
                postToNextFrame(idlingResource::decrement)
            }
    }

    override fun onCleared() {
        super.onCleared()

        disposable?.dispose()
    }
}
