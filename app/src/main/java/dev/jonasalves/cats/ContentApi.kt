package dev.jonasalves.cats

import dev.jonasalves.cats.model.Category
import dev.jonasalves.cats.model.Item
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface ContentApi {
    @GET("categories.json")
    fun categories(): Single<List<Category>>

    @GET("categories/{categoryId}.json")
    fun items(@Path("categoryId") categoryId: String): Single<List<Item>>
}