package dev.jonasalves.cats.widget

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dev.jonasalves.cats.content.CategoryVisitsCounter
import dev.jonasalves.cats.model.Category

/**
 * The most visited category will have spanSize = 3.
 * The second most visited category will have spanSize = 2.
 * All other categories will have spanSize = 1.
 */
class CategorySpanSizeLookup(
    private val categoryVisitCounter: CategoryVisitsCounter,
    private val recyclerView: RecyclerView
) : GridLayoutManager.SpanSizeLookup() {
    private val categories get() = (recyclerView.adapter as CategoriesAdapter).items!!

    override fun getSpanSize(position: Int): Int {
        val category = categories[position]
        val topCategories = categories.topVisited(categoryVisitCounter)

        return when (category) {
            topCategories.getOrNull(0) -> 3
            topCategories.getOrNull(1) -> 2
            else -> 1
        }
    }
}

fun List<Category>.topVisited(categoryVisitCounter: CategoryVisitsCounter) =
    asSequence()
        .map { it to categoryVisitCounter.visitsFor(it.id) }
        .filter { (_, visits) -> visits > 0 }
        .sortedByDescending { (_, visits) -> visits }
        .map { (category, _) -> category }
        .take(2)
        .toList()
