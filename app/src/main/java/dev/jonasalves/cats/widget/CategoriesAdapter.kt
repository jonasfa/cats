package dev.jonasalves.cats.widget

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dev.jonasalves.cats.databinding.PictureAndLabelAdapterItemBinding
import dev.jonasalves.cats.model.Category

class CategoriesAdapter(
    val items: List<Category>?,
    val onCategorySelected: (Category) -> Unit
) : RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = PictureAndLabelAdapterItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount() = items?.size ?: 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.apply {
            item = items!![position]
            executePendingBindings()
            root.setOnClickListener { onCategorySelected(item as Category) }
            root.requestLayout()
        }
    }

    class ViewHolder(val binding: PictureAndLabelAdapterItemBinding) : RecyclerView.ViewHolder(binding.root)

    companion object {
        @JvmStatic
        fun create(objects: List<Category>?, onCategorySelected: (Category) -> Unit) =
            CategoriesAdapter(objects, onCategorySelected)
    }
}
