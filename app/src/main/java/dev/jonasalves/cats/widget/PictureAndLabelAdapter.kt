package dev.jonasalves.cats.widget

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.databinding.DataBindingUtil
import dev.jonasalves.cats.databinding.PictureAndLabelAdapterItemBinding

class PictureAndLabelAdapter(
    context: Context,
    private val objects: List<PictureAndLabel>?
) : BaseAdapter() {
    override fun getCount() = objects?.size ?: 0
    override fun getItem(position: Int) = objects?.get(position)
    override fun getItemId(position: Int) = position.toLong()

    private val layoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val binding = convertView?.let { DataBindingUtil.getBinding<PictureAndLabelAdapterItemBinding>(it) }
            ?: PictureAndLabelAdapterItemBinding.inflate(layoutInflater, parent, false)

        binding.apply {
            item = getItem(position)
            executePendingBindings()
        }

        return binding.root
    }

    companion object {
        @JvmStatic
        fun create(context: Context, objects: List<PictureAndLabel>?) = PictureAndLabelAdapter(context, objects)
    }
}

interface PictureAndLabel {
    val label: String
    val imageUrl: String?
}
