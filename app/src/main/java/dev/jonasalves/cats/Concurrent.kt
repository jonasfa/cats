package dev.jonasalves.cats

import android.os.Handler
import android.os.Looper
import android.view.Choreographer

inline fun postToNextFrame(crossinline block: () -> Unit) {
    Handler(Looper.getMainLooper()).post {
        Choreographer.getInstance().postFrameCallback {
            block()
        }
    }
}
