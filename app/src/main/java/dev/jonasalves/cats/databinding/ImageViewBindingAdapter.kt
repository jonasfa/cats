package dev.jonasalves.cats.databinding

import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

object ImageViewBindingAdapter {
    @BindingAdapter("remoteImageUrl", "placeholderRes")
    @JvmStatic
    fun setRemoteImageUrl(view: ImageView, imageUrl: String?, @DrawableRes placeholderRes: Int) {
        val placeholder = AppCompatResources.getDrawable(view.context, placeholderRes)

        if (imageUrl == null) {
            Picasso.with(view.context)
                .cancelRequest(view)
            view.setImageDrawable(placeholder)
        } else {
            Picasso.with(view.context)
                .load(imageUrl)
                .placeholder(placeholder)
                .into(view)
        }
    }
}
