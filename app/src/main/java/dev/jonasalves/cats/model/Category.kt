package dev.jonasalves.cats.model

import dev.jonasalves.cats.widget.PictureAndLabel
import java.io.Serializable

data class Category(
    val id: String,
    val name: String,
    override val imageUrl: String? = null
) : Serializable, PictureAndLabel {
    override val label get() = name
}
