package dev.jonasalves.cats.model

import dev.jonasalves.cats.widget.PictureAndLabel
import java.io.Serializable

data class Item(
    val id: String,
    val title: String,
    override val imageUrl: String?,
    val quote: String
) : Serializable, PictureAndLabel {
    override val label get() = title
}
