package dev.jonasalves.cats

import android.app.Application
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dev.jonasalves.cats.injection.InjectionActivityCallbacks

open class Application : Application() {
    open val baseUrl = "https://jonasfa.gitlab.io/cats/api/"

    override fun onCreate() {
        super.onCreate()

        registerActivityLifecycleCallbacks(InjectionActivityCallbacks(this))
    }
}
