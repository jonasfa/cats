package dev.jonasalves.cats.content

open class CategoryVisitsCounter {
    private val visits = mutableMapOf<String, Int>()

    open fun incrementVisits(categoryId: String) {
        val previousVisitsCount = visits[categoryId] ?: 0
        visits[categoryId] = previousVisitsCount + 1
    }

    open fun visitsFor(categoryId: String) =
        visits[categoryId] ?: 0
}
