package dev.jonasalves.cats.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil.setContentView
import dev.jonasalves.cats.R
import dev.jonasalves.cats.databinding.ActivityMostVisitedCategoryBinding
import dev.jonasalves.cats.model.Category

class MostVisitedCategoryActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView<ActivityMostVisitedCategoryBinding>(this, R.layout.activity_most_visited_category).apply {
            category = intent.getSerializableExtra(EXTRA_CATEGORY) as Category
            pictureAndLabel.root.setOnClickListener { launchCategoryItems(category!!) }
            onLaunchCategories = ::launchCategories

            executePendingBindings()
        }
    }

    private fun launchCategoryItems(category: Category) {
        startActivity(
            Intent(this, CategoryItemsActivity::class.java)
                .putExtra(CategoryItemsActivity.EXTRA_CATEGORY, category)
        )
    }

    private fun launchCategories() {
        startActivity(
            Intent(this, CategoriesActivity::class.java)
                .putExtra(CategoriesActivity.EXTRA_PREVENT_MOST_VISITED, true)
        )
        finish()
    }

    companion object {
        const val EXTRA_CATEGORY = "category"
    }
}
