package dev.jonasalves.cats.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil.setContentView
import androidx.lifecycle.Transformations.map
import androidx.recyclerview.widget.GridLayoutManager
import dev.jonasalves.cats.R
import dev.jonasalves.cats.content.CategoryVisitsCounter
import dev.jonasalves.cats.databinding.ActivityCategoriesBinding
import dev.jonasalves.cats.injection.NeedsCategoriesViewModel
import dev.jonasalves.cats.injection.NeedsCategoryVisitsCounter
import dev.jonasalves.cats.model.Category
import dev.jonasalves.cats.viewmodel.CategoriesViewModel
import dev.jonasalves.cats.widget.CategorySpanSizeLookup
import dev.jonasalves.cats.widget.topVisited
import kotlin.math.max

class CategoriesActivity : AppCompatActivity(), NeedsCategoryVisitsCounter, NeedsCategoriesViewModel {
    override lateinit var categoryVisitCounter: CategoryVisitsCounter
    override lateinit var categoriesViewModel: CategoriesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val categoriesUnlessFinishing = map(categoriesViewModel.categories) { categories ->
            categories?.takeUnless(::launchMostVisitedCategory)
        }

        setContentView<ActivityCategoriesBinding>(this, R.layout.activity_categories).apply {
            lifecycleOwner = this@CategoriesActivity
            categories = categoriesUnlessFinishing
            onCategorySelected = ::launchCategoryItems
            categoriesSorter = ::sortCategoriesByVisits

            (recyclerView.layoutManager as GridLayoutManager).spanSizeLookup =
                CategorySpanSizeLookup(categoryVisitCounter, recyclerView)

            executePendingBindings()
        }
    }

    private fun launchMostVisitedCategory(categories: List<Category>): Boolean {
        categories.topVisited(categoryVisitCounter).firstOrNull()
            ?.takeUnless { intent.getBooleanExtra(EXTRA_PREVENT_MOST_VISITED, false) }
            ?.let { mostVisitedCategory ->
                startActivity(
                    Intent(this, MostVisitedCategoryActivity::class.java)
                        .putExtra(MostVisitedCategoryActivity.EXTRA_CATEGORY, mostVisitedCategory)
                )
                finish()
            }

        return isFinishing
    }

    private fun launchCategoryItems(category: Category) {
        categoryVisitCounter.incrementVisits(category.id)

        startActivity(
            Intent(this, CategoryItemsActivity::class.java)
                .putExtra(CategoryItemsActivity.EXTRA_CATEGORY, category)
        )
    }

    /**
     * Reorders a [List] of [Category] in order to avoid gaps in the UI grid when using [CategorySpanSizeLookup].
     */
    private fun sortCategoriesByVisits(categories: List<Category>?): List<Category>? {
        categories ?: return null

        val topCategories = categories.topVisited(categoryVisitCounter)

        return categories.toMutableList().apply {
            // move the most visited category to the first position
            topCategories.getOrNull(0)?.let {
                remove(it)
                add(0, it)
            }

            // move the second most visited category to the penultimate position
            topCategories.getOrNull(1)?.let {
                remove(it)
                add(max(1, size - 1), it)
            }
        }.toList()
    }

    companion object {
        const val EXTRA_PREVENT_MOST_VISITED = "prevent_most_visited"
    }
}
