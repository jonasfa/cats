package dev.jonasalves.cats.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil.setContentView
import dev.jonasalves.cats.R
import dev.jonasalves.cats.databinding.ActivityItemDetailsBinding
import dev.jonasalves.cats.model.Item
import dev.jonasalves.cats.tracking.Trackable

class ItemDetailsActivity : AppCompatActivity(), Trackable {
    override val trackableTypeAndId get() = kotlin.Pair("item", item.id)

    private val item get() = intent.getSerializableExtra(EXTRA_ITEM) as Item

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView<ActivityItemDetailsBinding>(this, R.layout.activity_item_details).apply {
            item = this@ItemDetailsActivity.item

            executePendingBindings()
        }
    }

    companion object {
        const val EXTRA_ITEM = "item"
    }
}
