package dev.jonasalves.cats.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat.makeSceneTransitionAnimation
import androidx.core.content.ContextCompat
import androidx.core.util.Pair
import androidx.databinding.DataBindingUtil.setContentView
import dev.jonasalves.cats.R
import dev.jonasalves.cats.databinding.ActivityCategoryItemsBinding
import dev.jonasalves.cats.injection.NeedsCategoryItemsViewModel
import dev.jonasalves.cats.model.Category
import dev.jonasalves.cats.model.Item
import dev.jonasalves.cats.tracking.Trackable
import dev.jonasalves.cats.viewmodel.CategoryItemsViewModel

class CategoryItemsActivity : AppCompatActivity(), NeedsCategoryItemsViewModel, Trackable {
    override val trackableTypeAndId get() = kotlin.Pair("category", category.id)
    override lateinit var categoryItemsViewModel: CategoryItemsViewModel

    private val category get() = intent.getSerializableExtra(EXTRA_CATEGORY) as Category

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        categoryItemsViewModel.category = category

        setContentView<ActivityCategoryItemsBinding>(this, R.layout.activity_category_items).apply {
            lifecycleOwner = this@CategoryItemsActivity
            viewModel = this@CategoryItemsActivity.categoryItemsViewModel
            onItemSelected = ::launchItemDetails

            executePendingBindings()
        }
    }

    private fun launchItemDetails(item: Item, view: View) {
        val picture = view.findViewById<View>(R.id.picture)

        ContextCompat.startActivity(
            this,
            Intent(this, ItemDetailsActivity::class.java).putExtra(ItemDetailsActivity.EXTRA_ITEM, item),
            makeSceneTransitionAnimation(this, Pair(picture, "picture")).toBundle()
        )
    }

    companion object {
        const val EXTRA_CATEGORY = "category"
    }
}
