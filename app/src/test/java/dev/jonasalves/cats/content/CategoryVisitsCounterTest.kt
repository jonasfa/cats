package dev.jonasalves.cats.content

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class CategoryVisitsCounterTest {
    val subject = CategoryVisitsCounter()

    @Test
    fun neverIncremented_hasZeroVisits() {
        assertThat(subject.visitsFor("abc"), `is`(0))
    }

    @Test
    fun afterOneIncrement_hasOneVisit() {
        subject.incrementVisits("abc")
        assertThat(subject.visitsFor("abc"), `is`(1))
    }

    @Test
    fun afterSevenIncrement_hasSevenVisit() {
        repeat(7) { subject.incrementVisits("abc") }
        assertThat(subject.visitsFor("abc"), `is`(7))
    }

    @Test
    fun differentIdsHaveDifferentCounts() {
        repeat(3) { subject.incrementVisits("abc") }
        repeat(2) { subject.incrementVisits("123") }

        assertThat(subject.visitsFor("abc"), `is`(3))
        assertThat(subject.visitsFor("123"), `is`(2))
    }
}
